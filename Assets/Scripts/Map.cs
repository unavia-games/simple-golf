﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Simple representation of a tile
public struct Tile
{
    // Indexes of vertices in mesh vertex array
    public int[] cornerVerticeIndexes;
    public int centerVertexIndex;
}


[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Map : MonoBehaviour
{
    public Camera Camera;
    public Vector2Int MapSize = new Vector2Int(5, 5);
    [Range(0.5f, 2f)]
    public float TileSize = 1f;
    public bool DebugGeneration = true;

    // Number of vertices used to draw triangulations for a tile (4 triangles * 3 vertices)
    private int TILE_VERTEXES = 12;

    private Vector3[] vertices;
    private Vector3[] originalVertices;
    private Mesh mesh;
    private MeshCollider meshCollider;

    private Vector3? hitPoint = null;
    private Tile? hitTile = null;

    void Awake()
    {
        StartCoroutine(Generate());
    }

    void Update()
    {
        RaycastHit hit;   
        Ray ray = Camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit)) {
            hitPoint = hit.point;
            hitTile = GetTileFromPoint(hit.point);

        } else {
            hitPoint = null;
            hitTile = null;
        }

        // TODO: Expand terrain management
        if (hitTile.HasValue)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Vector3[] normals = mesh.normals;
                vertices[hitTile.Value.centerVertexIndex] += Vector3.up;
                Array.ForEach(hitTile.Value.cornerVerticeIndexes, i => {
                    vertices[i] += Vector3.up;
                });

                // TODO: Move neighbouring tile centers to match (an average?)

                mesh.vertices = vertices;
                meshCollider.sharedMesh = null;
                meshCollider.sharedMesh = mesh;
            }
            else if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                Vector3[] normals = mesh.normals;
                vertices[hitTile.Value.centerVertexIndex] += Vector3.down;
                Array.ForEach(hitTile.Value.cornerVerticeIndexes, i => {
                    vertices[i] += Vector3.down;
                });

                // TODO: Move neighbouring tile centers to match (an average?)

                mesh.vertices = vertices;
                meshCollider.sharedMesh = null;
                meshCollider.sharedMesh = mesh;
            }
        }
    }


    private IEnumerator Generate()
    {
        WaitForSeconds wait = new WaitForSeconds(0.01f);

        mesh = new Mesh();
        mesh.name = "Procedural Map";
        GetComponent<MeshFilter>().mesh = mesh;

        // Each dimension needs one additional vertice than tiles
        int cornerVerticeCount = (MapSize.x + 1) * (MapSize.y + 1);
        int centerVerticeCount = MapSize.x * MapSize.y;

        vertices = new Vector3[cornerVerticeCount + centerVerticeCount];
        int verticeIndex = 0;
        for (int y = 0; y <= MapSize.y; y++) {
            // Generate row's corner vertices
            for (int x = 0; x <= MapSize.x; x++) {
                vertices[verticeIndex] = new Vector3(x * TileSize, 0, y * TileSize);
                verticeIndex++;

                // Generation time can be debugged by slowing down vertice generation
                if (DebugGeneration) {
                    mesh.vertices = vertices;
                    yield return wait;
                }
            }

            // Generate row's accompanying center vertices
            if (y != MapSize.y)
            {
                for (int x = 0; x < MapSize.x; x++)
                {
                    vertices[verticeIndex] = new Vector3(x * TileSize + TileSize / 2, 0, y * TileSize + TileSize / 2);
                    verticeIndex++;
                }

                // Generation time can be debugged by slowing down vertice generation
                if (DebugGeneration) {
                    mesh.vertices = vertices;
                    yield return wait;
                }
            }
        }

        int[] triangles = new int[MapSize.x * MapSize.y * TILE_VERTEXES];
        int triangleIndex = 0;
        verticeIndex = 0;
        for (int y = 0; y < MapSize.y; y++) {
            for (int x = 0; x < MapSize.x; x++) {
                // Left triangle
                triangles[triangleIndex] = verticeIndex;
                triangles[triangleIndex + 1] = verticeIndex + 2 * MapSize.x + 1;
                triangles[triangleIndex + 2] = verticeIndex + MapSize.x + 1;
                // Top triangle
                triangles[triangleIndex + 3] = verticeIndex + 2 * MapSize.x + 1;
                triangles[triangleIndex + 4] = verticeIndex + 2 * MapSize.x + 2;
                triangles[triangleIndex + 5] = verticeIndex + MapSize.x + 1;
                // Right triangle
                triangles[triangleIndex + 6] = verticeIndex + 2 * MapSize.x + 2;
                triangles[triangleIndex + 7] = verticeIndex + 1;
                triangles[triangleIndex + 8] = verticeIndex + MapSize.x + 1;
                // Bottom triangle
                triangles[triangleIndex + 9] = verticeIndex + 1;
                triangles[triangleIndex + 10] = verticeIndex;
                triangles[triangleIndex + 11] = verticeIndex + MapSize.x + 1;

                triangleIndex += TILE_VERTEXES;
                verticeIndex++;

                // Generation time can be debugged by slowing down triangle generation
                if (DebugGeneration) {
                    mesh.triangles = triangles;
                    yield return wait;
                }
            }

            // Must skip last corner vertex in row AND following row of center vertices
            verticeIndex += MapSize.x + 1;
        }

        // Update mesh with generated vertices and triangles
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        // Add mesh collider to mesh
        meshCollider = gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;

        // Useful if resetting vertices to original positions
        originalVertices = vertices;

        // DEBUG
        // animator = Animate();
        // StartCoroutine(animator);
    }

    private Tile GetTileFromPoint(Vector3 point)
    {
        // Get "x" and "y" coordinates of selected tile (need to convert to vertices later)
        int targetX = (int) Math.Floor(point.x * (1 / TileSize));
        int targetY = (int) Math.Floor(point.z * (1 / TileSize));

        int bottomLeftVertex = targetY * (MapSize.x + 1 + MapSize.x) + targetX;
        int topLeftVertex = bottomLeftVertex + 2 * MapSize.x + 1;
        int topRightVertex = topLeftVertex + 1;
        int bottomRightVertex = bottomLeftVertex + 1;
        int centerVertex = bottomLeftVertex + MapSize.x + 1;

        return new Tile()
        {
            cornerVerticeIndexes = new int[4] { bottomLeftVertex, topLeftVertex, topRightVertex, bottomRightVertex },
            centerVertexIndex = centerVertex
        };
    }

    // Whether a provided vertice index represents a corner vertex (or center)
    private bool IsCornerVertex(int verticeIndex)
    {
        int vertexCount = MapSize.x + 1;
        int centerCount = MapSize.x;

        return verticeIndex % (vertexCount + centerCount) <= MapSize.x;
    }


    private void OnDrawGizmos() {
        if (vertices == null) return;

        // Draw the vertices (distinguish between corner and center vertices)
        float vertexSize = 0.1f;
        for (int i = 0; i < vertices.Length; i++) {
            if (IsCornerVertex(i))
            {
                Gizmos.color = Color.black;
                vertexSize = 0.1f;
            }
            else
            {
                Gizmos.color = Color.grey;
                vertexSize = 0.05f;
            }
            Gizmos.DrawSphere(transform.TransformPoint(vertices[i]), vertexSize);
        }

        // Indicate position mouse targets the mesh
        if (hitPoint.HasValue) {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(hitPoint.Value, 0.1f);
        }

        // Indicate target tile vertices
        if (hitTile.HasValue)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(transform.TransformPoint(vertices[hitTile.Value.centerVertexIndex]), 0.1f);

            Gizmos.color = Color.blue;
            Array.ForEach(hitTile.Value.cornerVerticeIndexes, i =>
            {
                Gizmos.DrawSphere(transform.TransformPoint(vertices[i]), 0.1f);
            });
        }
    }
}
